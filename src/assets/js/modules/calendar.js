import $ from 'jquery'
import pickmeup from 'pickmeup' 

const Module = (function(){
    // difinded elements for events
    let $input = 'input.deadLine'
    
    // return methods module
    return {
        runPic(){
            pickmeup($input, {
                format: 'd-m-Y',
                hide_on_select: true,
                min: new Date
            });
        }, 
        init(){
            this.runPic()
        }
    }

})()

export default Module 