import $ from 'jquery'

const Module = (function(){
    // difinded elements for events
    let $body
    
    // difinded elements for manipulatuon
    
    // return methods module
    return {
        caching () {
            $body = $('body')
        },
        changeText(e){
            let $el = $(e.currentTarget)
            let val =  $el.val()
            val = val.split('\\').pop();
            $el.parents('.field-attach').find('span').text(val)
            $el.trigger('blur')
        }, 
        events(){
            $body.on('change', '.js-attach', this.changeText.bind(this));
        },
        init(){
            this.caching()
            this.events()
        }
    }

})()

export default Module 