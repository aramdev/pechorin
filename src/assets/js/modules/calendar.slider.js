import $ from 'jquery';
import 'owl.carousel.es6'


const Module = (function(){
    // difinded elements for events
    let $body
        
    // difinded elements for manipulatuon
    let $slider,
        $item,
        $list

    // return methods module
    return {
        caching () {
            $body = $('body')
            $slider = $('.calDays')
            $item = $('.js-changeEvn')
            $list = $('.dayEvn')
        },
        runCorusel (){
            $slider.owlCarousel({
                loop:true,
                nav:true,
                dots: false,
                margin: 0,
                navText: ['<i class="icn-angle-left"></i>', '<i class="icn-angle-right"></i>'],
                responsive:{
                    0:{
                        items:3,
                    },
                    767:{
                        items:5,
                    },
                    991:{
                        items:7,
                    },
                    1200:{
                        items:14,
                    }
                }
            });
        },
        
        changeEvn (e) {
            let $el = $(e.currentTarget)
            if(!$el.hasClass('active')){
                $item.removeClass('active')
                $el.addClass('active')
                $list.fadeOut(500, function() {
                    $list.fadeIn(500);
                });

            }
        },
        events(){
            $body.on('click', '.js-changeEvn', this.changeEvn.bind(this));
        },
        init(){
            this.caching()
            this.runCorusel()
            this.events()
        }
    }

})()

export default Module 

