import $ from 'jquery'

const Module = (function(){
    // difinded elements for events
    let $body
       
    
    // difinded elements for manipulatuon
    let $slider

    // return methods module
    return {
        caching () {
            $body = $('body')
            $slider = $('.redBlock__slider')
        },
        runSlider(e){
            let $el = $(e.currentTarget)
            let $items = $slider.find('.item')
            $items.each(function() {
                let num = $(this).attr('data-pos');
                num = parseInt(num)
                if(num == 1){
                    num = 3
                }else{
                    num--
                }
                $(this).attr('data-pos', num)
            });

            let current = $slider.find('.item[data-pos="1"]').attr('data-num');
            $el.prev('span').text(current)
            let next = $slider.find('.item[data-pos="2"]').attr('data-num');
            $el.text('/ '+next)
        }, 
        events(){
            $body.on('click', '.js-slideFigure', this.runSlider.bind(this));
        },
        init(){
            this.caching()
            this.events()
        }
    }

})()

export default Module 

