import autosize from 'autosize'

const Module = (function(){
    // return methods module
    return {
        autosize(){
            autosize(document.querySelectorAll('textarea'))
        }, 
        init(){
            this.autosize()
        }
    }

})()

export default Module 

