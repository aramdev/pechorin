import $ from 'jquery'
import Form from '../classes/form.js'
import './add.methods.js'


const Module = (function(){
    // return methods module
    return {
        allFormValidate(){
            (new Form("#addContest")).validation();
            (new Form("#manuscript")).validation();
        }, 
        init(){
            this.allFormValidate()
        }
    }

})()

export default Module 