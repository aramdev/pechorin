import $ from 'jquery';
import 'owl.carousel.es6'


const Module = (function(){
    // difinded elements for events
    
        
    // difinded elements for manipulatuon
    let $slider

    // return methods module
    return {
        caching () {
            $slider = $('.sliderHome')
        },
        runCorusel: function(){
            $slider.owlCarousel({
                loop:true,
                nav:true,
                dots: true,
                dotsEach: true,
                margin: 30,
                navText: ['<i class="icn-angle-left"></i>', '<i class="icn-angle-right"></i>'],
                responsive:{
                    0:{
                        items:1,
                    },
                    991:{
                        items:3,
                    }
                }
            });
        },
        init(){
            this.caching()
            this.runCorusel()
        }
    }

})()

export default Module 

