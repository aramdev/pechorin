import $ from 'jquery'

const Module = (function(){
    // difinded elements for events
    let $body
        
    // difinded elements for manipulatuon
     

    // return methods module
    return {
        caching () {
            $body = $('body')
        },
        toggleBlock(e){
            let $el = $(e.currentTarget)
            
            if($el.hasClass('open')){
                let txt = $el.attr('data-close')
                $el.removeClass('open')
                $el.parents('.toggleItem').find('.toggleItem__content').slideUp()
                $el.html(txt + '<i class="icn-angle-down"></i>')
            }else{
                let txt = $el.attr('data-open')
                $el.addClass('open')
                $el.parents('.toggleItem').find('.toggleItem__content').slideDown()
                $el.html(txt + '<i class="icn-angle-down"></i>')
            }
        },
        events(){
            $body.on('click', '.js-toggleBlock', this.toggleBlock.bind(this));
        },
        init(){
            this.caching()
            this.events()
        }
    }

})()

export default Module 