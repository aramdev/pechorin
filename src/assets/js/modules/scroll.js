import $ from 'jquery'
import 'jquery-mousewheel'
import 'malihu-custom-scrollbar-plugin'
//import '../../../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js'



const Module = (function(){
    // difinded elements for events
    
        
    
    // difinded elements for manipulatuon
    let $scroll

    // return methods module
    return {
        caching () {
            $scroll = $('.js-scroll')
        },
        runScroll(){
            $scroll.mCustomScrollbar({
                scrollInertia: 1,
                documentTouchScroll: true,
                mouseWheel: {
                    preventDefault: false,
                }
            });
        }, 
        init(){
            this.caching()
            this.runScroll()
        }
    }

})()

export default Module 

