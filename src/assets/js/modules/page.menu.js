import $ from 'jquery'

const Module = (function(){
    // difinded elements for events
    let $body
        
    
    // difinded elements for manipulatuon
    let $menu

    // return methods module
    return {
        caching () {
            $body = $('body')
            $menu = $('.pageMenu__list')
        },
        toggleMenu(e){
            let $el = $(e.currentTarget)
            $menu.toggleClass('open');
            $el.toggleClass('open');
        },
        events(){
            $body.on('click', ".js-pageMenu", this.toggleMenu.bind(this));
        },
        init(){
            this.caching()
            this.events()
        }
    }

})()

export default Module 

