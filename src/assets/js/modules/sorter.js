import $ from 'jquery'

const Module = (function(){
    // difinded elements for events
    let $body
        
    
    // difinded elements for manipulatuon
    let $sorter

    // return methods module
    return {
        caching () {
            $body = $('body')
            $sorter = $('.js-sorter a')
        },
        sort(e){
            let $el = $(e.currentTarget)
            if($el.hasClass('active')){
                $el.toggleClass('dasc');
            }else{
                $sorter.removeClass('active dask')
                $el.addClass('active')
            }
        },
        events(){
            $body.on('click', '.js-sorter a', this.sort.bind(this));
        },
        init(){
            this.caching()
            
            this.events()
        }
    }

})()

export default Module 