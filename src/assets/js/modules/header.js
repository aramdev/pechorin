import $ from 'jquery'

const Module = (function(){
    // difinded elements for events
    let $body
        
    
    // difinded elements for manipulatuon
    let $header,
        $search

    // return methods module
    return {
        caching () {
            $body = $('body')
            $header = $('.header')
            $search = $('.mainSearch')
        },
        toggleMenu (e) {
            this.hideSearch()
            e.stopPropagation()
            $header.toggleClass('open');
        },
        hideMenu () {
            $header.removeClass('open');
        },
        toggleSearch (e) {
            this.hideMenu()
            e.stopPropagation()
            let $el = $(e.currentTarget)
            $el.toggleClass('open')
            $search.toggleClass('open')
        },
        hideSearch () {
            $('.js-search').removeClass('open')
            $search.removeClass('open')  
        },
        resetHeader () {
            this.hideMenu()
            this.hideSearch()
        },
        events(){
            $body.on('click', '.js-menu', this.toggleMenu.bind(this));
            $body.on('click', '.js-closeMenu', this.hideMenu.bind(this));
            $body.on('click', '.mainMenu__nav', function(e){e.stopPropagation()});
            $body.on('click', '.mainSearch', function(e){e.stopPropagation()});
            $body.on('click', '.js-search', this.toggleSearch.bind(this));
            $body.on('click', this.resetHeader.bind(this));
        },
        init(){
            this.caching()
            this.events()
        }
    }

})()

export default Module 

