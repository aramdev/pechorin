import $ from 'jquery'

const Module = (function(){
    // difinded elements for events
    let $body
        
    // difinded elements for manipulatuon
    let $btn 

    // return methods module
    return {
        caching () {
            $body = $('body')
            $btn = $('.js-toggleComments')
        },
        toggleBlock(e){
            e.stopPropagation()
            let $el = $(e.currentTarget)
            $el.toggleClass('open')
        },
        events(){
            $body.on('click', '.js-toggleComments', this.toggleBlock.bind(this));
        },
        init(){
            this.caching()
            this.events()
        }
    }

})()

export default Module 