import $ from 'jquery'

const Module = (function(){
    // difinded elements for events
    let $body
        
    // difinded elements for manipulatuon
     

    // return methods module
    return {
        caching () {
            $body = $('body')
        },
        toggleBlock(e){
            let $el = $(e.currentTarget)
            let el = $el.attr('data-toggle')
            
            if($el.hasClass('open')){
                $el.removeClass('open')
                $(el).slideUp()
            }else{
                let txt = $el.attr('data-toggle')
                $el.addClass('open')
                $(el).slideDown()
                
            }
        },
        events(){
            $body.on('click', '.js-toggleCard', this.toggleBlock.bind(this));
        },
        init(){
            this.caching()
            this.events()
        }
    }

})()

export default Module 