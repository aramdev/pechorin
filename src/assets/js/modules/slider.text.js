import $ from 'jquery';
import 'owl.carousel.es6'


const Module = (function(){
    // difinded elements for events
    
        
    // difinded elements for manipulatuon
    let $slider

    // return methods module
    return {
        caching () {
            $slider = $('.textSlider')
        },
        runCorusel: function(){
            $slider.owlCarousel({
                loop:true,
                nav:true,
                dots: true,
                margin: 0,
                navText: ['<i class="icn-angle-left"></i>', '<i class="icn-angle-right"></i>'],
                items: 1
            });
        },
        init(){
            this.caching()
            this.runCorusel()
        }
    }

})()

export default Module 

