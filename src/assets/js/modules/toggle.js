import $ from 'jquery'

const Module = (function(){
    // difinded elements for events
    let $body,
        $cnt
        
    // difinded elements for manipulatuon
    let $btn 

    // return methods module
    return {
        caching () {
            $body = $('body')
            $btn = $('.js-toggle')
            $cnt = $('.js-toggle').next()
        },
        toggleBlock(e){
            e.stopPropagation()
            let $el = $(e.currentTarget)
            if($el.hasClass('open')){
                $el.removeClass('open')
            }else{
                $btn.removeClass('open')
                $el.addClass('open');
            }
        },
        hideBlock () {
            $btn.removeClass('open')
        },
        events(){
            $body.on('click', '.js-toggle', this.toggleBlock.bind(this));
            $cnt.on('click',  function(e){e.stopPropagation()});
            $body.on('click', this.hideBlock.bind(this));
        },
        init(){
            this.caching()
            this.events()
        }
    }

})()

export default Module 