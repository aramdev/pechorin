import $ from 'jquery'

const Module = (function(){
    let x,y 

    // difinded elements for events
    let $body
        
    
    // difinded elements for manipulatuon
    let $audio,
        $playAudio,
        $video,
        $playVideo

    // return methods module
    return {
        caching () {
            $body = $('body')
            $audio = $('audio')
            $video = $('.video')
            $playAudio = $('.js-play-audio') 
            $playVideo = $('.js-play-video')
        },
        playAudio (e) {
            let $el = $(e.currentTarget)
            let $currenAudio = $el.parent('.musicItem').find('audio')
            if($el.hasClass('icn-music')){
                this.stopMedia()

                $el.removeClass('icn-music').addClass('icn-pause')
                $currenAudio.trigger('play')                
            
            }else{
                $el.removeClass('icn-pause').addClass('icn-music')
                $currenAudio.trigger('pause')
            }
        },
        playVidoe (e) {
            let $el = $(e.currentTarget)
            let $parent = $el.parent('.video')

            this.stopMedia()
            $parent.addClass('active')
            $parent.find('video').attr('controls', true).trigger('play')
        },
        stopMedia () {
            $playAudio.removeClass('icn-pause').addClass('icn-music')
            $audio.trigger('pause')
            $video.removeClass('active')
            $video.find('video').trigger('pause').removeAttr('controls')

        },
        setTimeLine(e) {
            let $el = $(e.currentTarget)

            let progress = (e.currentTarget.currentTime) * 100 / (e.currentTarget.duration)
            let $timeline = $el.parent('.musicItem').find('.timeline')
            $timeline.addClass('active')
            $timeline.find('.progres').css({width: progress+'%'})
        },
        hideTimeLine (e) {
            let $el = $(e.currentTarget)
            $el.parent('.musicItem').find('.timeline').removeClass('active')
            $el.parent('.musicItem').find('.js-play-audio').removeClass('icn-pause').addClass('icn-music')
        },
        changeTime (e) {
            let $el = $(e.currentTarget)
            let offset = $el.offset();
            let width = $el.width()
            x = e.pageX - offset.left;
            
            let pos = x * 100 / width

            let $audio = $el.parents('.musicItem').find('audio')

            
            $audio.each(function(index, el) {
                let current = el.duration / 100 * pos
                el.currentTime = current
            });
            
            
        },
        events(){
            $body.on('click', '.js-play-audio', this.playAudio.bind(this));
            $body.on('click', '.js-play-video', this.playVidoe.bind(this));
            $body.on('click', '.musicItem__name .timeline .bg', this.changeTime.bind(this));
            $audio.bind('timeupdate', this.setTimeLine.bind(this));
            $audio.bind('pause', this.hideTimeLine.bind(this));
        },
        init(){
            this.caching()
            this.events()
        }
    }

})()

export default Module 






