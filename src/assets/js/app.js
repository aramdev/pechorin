import $ from 'jquery'
import 'bootstrap'
import 'owl.carousel.es6'
import 'selectize' 

// MODULES
import autosize from './modules/autosize.js'
import header from './modules/header.js'
import slider from './modules/slider.large.js'
import scroll from './modules/scroll.js'
import calendarSlider from './modules/calendar.slider.js'
import textSlider from './modules/slider.text.js'
import homeSlider from './modules/slider.home.js'
import figureSlider from './modules/slider.figure.js'
import pageMenu from './modules/page.menu.js'
import toggle from './modules/toggle.js'
import toggleBlock from './modules/toggle.block.js'
import toggleCard from './modules/toggle.card.js'
import comments from './modules/comments.js'
import select from './modules/select.js'
import sorter from './modules/sorter.js'
import calendar from './modules/calendar.js'
import attach from './modules/attach.js'
import media from './modules/media.js'
import formValidate from './modules/form.validate.js'


// MODULES INIT
$(function(){
	autosize.init()
	attach.init()
	header.init()
	slider.init()
	scroll.init()
	calendarSlider.init()
	textSlider.init()
	homeSlider.init()
	figureSlider.init()
	pageMenu.init()
	toggle.init()
	comments.init()
	select.init()
	sorter.init()
	toggleBlock.init()
	formValidate.init()
	calendar.init()
	toggleCard.init()
	media.init()

})


