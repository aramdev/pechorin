'use strict'

const gulp     = require('gulp');
const $        = require('gulp-load-plugins')();


const webpack = require('webpack'); 
const webpackStream = require('webpack-stream');
const compiler = require('webpack')


let isDev = true;
let isProd = !isDev;

let webConfig = {
	output: {
		filename: 'app.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: '/node_modules/'
			}
		]
	},
	plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
    ],
	mode: isDev ? 'development' : 'production',
	devtool: isDev ? 'eval-source-map' : 'none'
}

module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
	        .pipe(webpackStream(webConfig, compiler))
			.pipe(gulp.dest(options.dest))
	};
};







