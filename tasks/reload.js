'use strict'

const gulp        = require('gulp');
const browserSync = require('browser-sync')

module.exports = function(options) {
	return function(){
		browserSync.init({
	        server: options.server
	    });
	    browserSync.watch(options.allFiles).on('change', browserSync.reload)
	};
	
};


