'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();

module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
	        .pipe($.include())
	        .pipe($.cached(options.taskName))
	        .pipe($.rubyHaml({
	            doubleQuote: true, 
	            encodings: "UTF-8",
	        }).on('error', function(e) { console.log(e.message); }))
	        .pipe($.cached(options.taskName))
	        .pipe($.htmlBeautify({
	            indent_char: '\t', 
	            indent_size: 1
	        }))
	        .pipe(gulp.dest(options.dest));
	};
};