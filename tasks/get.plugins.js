'use strict'

const gulp = require('gulp');
const $ = require('gulp-load-plugins')();

module.exports = function(options) {
	return function(){
		return gulp.src($.npmDist(), {base:'./node_modules'})
    		.pipe(gulp.dest(options.dest));
	};
};